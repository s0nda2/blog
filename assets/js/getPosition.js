/**
 * getPosition.js
 */


/**
 * 
 * Implementation of getPosition function according to instructions described
 * in http://javascript.info/tutorial/coordinates
 *
 * This function determines the x- and y-position of reference element 
 * in HTML document and is used for older browsers than IE 9.
 *
 * @params :
 *	el : Reference element the position of which is to be determined
 *
 * @return :
 *	{ x, y } : an two-element bundle containing the x- and y-position
 *				of the reference element in HTML document.
 */
function getPositionClassic ( el ) {
	var left = 0, top = 0;
	while ( el ) {
		left += el.offsetLeft;
		top += el.offsetTop;
		el = el.offsetParent; // returns a read-only reference of the closest relatively 
		 			// positioned parent-element (offsetParent) of an HTMLElement
	}
	return { x : left, y : top };
} // getPositionClassic


/*
 * http://javascript.info/tutorial/coordinates
 * MSDN : https://msdn.microsoft.com/en-us/library/hh781509(v=vs.85).aspx
 * MDN  : https://developer.mozilla.org/de/docs/Web/API/Element/getBoundingClientRect
 * 	   https://developer.mozilla.org/en-US/docs/Web/API/Window/scrollY
 * 	   https://developer.mozilla.org/en-US/docs/Web/API/Window/pageXOffset
 *
 * This function determines the x- and y-position of reference element 
 * in HTML document and is used for both older IE (>= 4) and modern browsers.
 *
 * @params :
 *	  el : Reference element the position of which is to be determined
 *
 * @return :
 *	  { x, y } : an two-element bundle containing the x- and y-position
 *		    	 of the reference element in HTML document.
 */
function getPositionRect ( el ) {
	if ( !el.getBoundingClientRect() )  return null;
	
	var rect = el.getBoundingClientRect(); // DOMrect with 4 properties : left, top, right, bottom
	var top, left, scrollLeft, scrollTop, shiftLeft, shiftTop;
	
	scrollLeft = scrollTop = shiftLeft = shiftTop = 0;
	    
	// 1. Calculates the number of pixels the document has already been scrolled vertically (horizontally).
	//    For cross-browser compatibility, one uses window.pageXOffset and window.pageYOffset instead of 
	//    window.scrollX (no support in IE) and window.scrollY whenever it is possible.
	//    If older versions IE (< 9) don't support the property window.pageXOffset (window.pageYOffset),
	//    one can use Element.scrollLeft (Element.scrollTop) instead.
	//    Element.scrollLeft (.scrollTop) has been supported in FF, Safari, Chrome and IE (>= 6),
	//    while window.scrollX (.scrollY) is first available since IE 9.
	var supportPageOffset = ( window.pageXOffset !== undefined ) && ( window.pageYOffset !== undefined );
	// Check if the browser supports standards-compliant mode (CSS1Compat) and not the quirk mode (BackCompat).
 	// isCSSCompat is only TRUE if <!DOCTYPE html> is set. Otherwise FALSE, i.e. BackCompat (quirk mode).
 	var isCSS1Compat = (( document.compatMode || "" ) === "CSS1Compat");
 	// If <!DOCTYPE > is set (does exist), then the scroll can be taken from document.documentElement (<html>).
	// Otherwise from document.body (<body>) or document.body.parentNode.
	scrollLeft = supportPageOffset ? window.pageXOffset : isCSS1Compat ? document.documentElement.scrollLeft
						       : ( document.body.parentNode || document.body ).scrollLeft;
	scrollTop = supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop
							: ( document.body.parentNode || document.body ).scrollTop;

	// 2. Determines and calculates the top (left) offset between the rectangle, that surrounds the element el,
	//    and the most top (left) border of the viewport (window / current visible area).
	//
	// rect.top;
	// rect.left;
	
	// 3. The document (<html>, <body>) could be shifted from the top(left)-upper side, e.g. by using margin, padding ...
	//    Therefore, calculates the shift values (== border-width).
	shiftLeft = ( document.documentElement || document.body.parentNode || document.body ).clientLeft || 0;
	shiftTop= ( document.documentElement || document.body.parentNode || document.body ).clientTop || 0;
	
	// 4. Calculates the coordinates of the reference element el.
	left = scrollLeft + rect.left - shiftLeft;
	top = scrollTop + rect.top - shiftTop;
	
	return { x : left, y : top };	
} // getPositionRect
 
 
/* 
 * This function determines the x- and y-position of reference element 
 * in HTML document.
 *
 * @params :
 *	  el : Reference element the position of which is to be determined
 *
 * @return :
 *	  { x, y } : an two-element bundle containing the x- and y-position
 *		    	 of the reference element in HTML document.
 */
function getPosition ( el ) {
	return ( el.getBoundingClientRect() ) ? /* IE 9+ */ getPositionRect ( el ) 
					      : /* older */ getPositionClassic ( el );
} // getPosition
